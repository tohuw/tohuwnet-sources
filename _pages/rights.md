---
layout: page
title: "Rights"
permalink: rights/
redirect_from: /license/
redirect_to:
menuitem: false
icon:
---
<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Tohuw.Net</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://tohuw.net" property="cc:attributionName" rel="cc:attributionURL">Ron Scott-Adams</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

The type font faces on this site are:

- [Fira Sans](https://github.com/mozilla/Fira), created by [Mozilla](https://www.mozilla.org/en-US/) and used under the [SIL Open Font License](https://raw.githubusercontent.com/mozilla/Fira/73645a967e9bfc8fc9c67d630cff517e781a0590/LICENSE).
- [Metropolis](https://github.com/chrismsimpson/Metropolis) created by [Chris Simpson]() and used under the [SIL Open Font License](https://raw.githubusercontent.com/chrismsimpson/Metropolis/813a8382e5821a200486404e535ec7f5cc2a6b03/Open%20Font%20License.md)

The icon font face is [Font Awesome](http://fontawesome.io), created by [Dave Gandy](http://twitter.com/davegandy) and used under the [SIL Open Font License](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).

The [header, footer, and menu background](https://pixabay.com/en/desert-drought-dehydrated-clay-soil-279862/) is the work of [Marion](https://pixabay.com/en/users/_Marion-36647) and used under a [Public Domain license](https://pixabay.com/en/service/terms/#usage).

The [search results background](https://pixabay.com/en/scotland-highlands-and-islands-1645868/) is the work of [Thomas Ulrich](https://pixabay.com/en/users/LoboStudioHamburg-13838/) and used under a [Public Domain license](https://pixabay.com/en/service/terms/#usage).

All other media and works are property of their attributed creators or owners as specified within the content and/or source in which they appear. If you believe any content of any kind is used in violation of licenses, agreements, or legal restrictions, please [contact Ron Scott-Adams](mailto:ron@touw.net).
