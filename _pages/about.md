---
layout: page
title: About
permalink: ron-scott-adams/
redirect_from: /about/
redirect_to:
menuitem: true
icon: question-circle
---

> We're stuck with technology when what we really want is just stuff that works.
> 
> (Douglas Adams)

{% figure class:"figure-aside" caption:"Shudder. Just, bleh. (Image credit: Unknown)" %}
{% img '{{image_path}}coffee_foam_cup.png' alt:'Coffee in a cheap foam cup' %}
{% endfigure %}
I hate coffee in cheap foam cups. The unsettling way it seeps through the pores (longing to escape its possibly carcinogenic confines) is a visceral reminder of the taste: stale, plastic, and hot without warmth. Don't mistake this diatribe for elitism; I'm not too proud to down a Folger's. I've a taste for a well-made Sumatran pour-over, but not a requirement for one. I recognize the reality and utility of a fast cup of coffee to get the job done: put warmth in the bones and caffeine in the bloodstream. I don't need cream, sugar, *et al*; I don't believe in depriving oneself of savor, even if it is the less pleasant bitterness of a cheap coffee, it is still a thing to be *experienced*. Just, don't put it in those horrid foam cups, okay?

Now that we're through that, I trust we have a better understanding.

---

## What is Tohuw.Net About?
{% pullquote %}
My job has nothing to do with coffee, but if you're in my line of work you sometimes need to have strong opinions about things like coffee. I'm a Hyper-Converged Systems Engineer at VMware. I help people figure out how [vSAN](http://www.vmware.com/products/vsan.html) and VMware's HCI[^hci] solutions can help make their infrastructures better. Through this, I have the privilege of working with key enterprise and government entities across the U.S. as they modernize datacenters and mindsets alike.

So, you'll read a healthy dose of HCI and storage virtualization stuff here. But I also like to think about the technical and philosophical aspects of today's technology environments. I even more like hearing and sharing similar thoughts with others. I find we IT humans are an overall philosophical, humorous lot[^dasblinkenlights]. I think the humor comes from reconciling the infuriating duality of what seems a rigorous, scientific process sort of job with a frank realization that there's a definitely unscientific art to managing, designing, or developing these things. This happens because people are involved, and though we're sometimes reluctant to admit it, this is what makes the work actually interesting to do.

I aim to write simply, because the goal is to appeal to a wide range of technical backgrounds and skill levels. {" The best leaps forward in my career and knowledge have been catalyzed by people with a great deal of experience and insight graciously inviting me to a seat at the table, then promptly throwing me to the sharks. "} Ideally, I hope to spark discussions (or outrage) among enough people that I end up somewhere in the middle, with the revered technical gurus out there to catalyze me further in my learning and shark-swimming, and to come across those earlier in their journey to encourage and lend a hand. (A hand for helping and pushing into the sharky waters, of course.) Failing that, at least I'll have recorded some hopefully relevant stuff.
{% endpullquote %}

---

## What's a Tohuw?
It's a Hebrew word that appears several times in the Bible. There's no succinct English translation. [Larry Pierce sums up the Biblical usage of *tohuw*](https://www.blueletterbible.org/lang/lexicon/lexicon.cfm?Strongs=H8414&t=ESV) like so:

> 1. formlessness, confusion, unreality, emptiness
> 	1. formlessness (of primeval earth)
> 		1. nothingness, empty space
> 	2. that which is empty or unreal (of idols) *(fig)*
> 	3. wasteland, wilderness (of solitary places)
> 	4. place of chaos
> 	5. vanity

I've long used it as a moniker with the full intention of finally making a blog of the same name. The word strikes me as a meaningful comparison of the sweeping disorder, the quiet refuges, and the empty grasps at straws we face in our short lives. Technology is fascinating because it is a heartfelt expression of man's desire for order, while at the same time bringing a new and profound chaos which must be further worked out. This is no reason for despair or frustration; the price of technological progress is worth the cost of admission. There's always failures and detriments along the way, but optimism is the fuel for better technology, and conversation is the spark to light that fuel.

So welcome to this place of chaos, this wilderness, this *tohuw*. Let's talk.

[^hci]: Hyper-Converged Infrastructure, a happy little buzzword mostly meaning: "make your virtualization gear do your storage work too"
[^dasblinkenlights]: I'd be remiss to not recall here "[DAS BLINKENLIGHTS](https://wikipedia.org/wiki/Blinkenlights)", which sums up the regretful kind of humor from which my profession stems.
