---
layout: post
title: "Must-See VMworld 2017 HCI Sessions"
slug: "vmworld-2017-hci-sessions"
summary: "A quick rundown of several HCI sessions I and others are presenting for VMworld 2017"
date: 2017-08-13 19:00
updated: 2017-08-14 23:03
editreason: Microsoft's Browsers still have deficient HTML standards support. :(
topic: vmworld
tags: vmworld vsan events
thumbnail: "vmworld2017-logo.gif"
nowlistening: "Europe – The Final Countdown"
comments: true
---
{% pullquote %}
Given that [VMworld 2017](https://vmworld.com) is nearly a dozen days away, I thought I ought to write up a quick summary of my three sessions, plus a few others you shouldn't miss. If you've been to VMworld before, you probably know that trying to get to every session you want to see is nothing short of overwhelming. Rather than try to pack 100% of your time with sessions (although you easily could), I suggest leaving plenty of time to meet people – talk with customers, partners, and VMware folks about your environment, your ideas, whatever. {" While VMworld has the best sessions on the planet, some of the most enriching times I spent there have been side conversations in hallways. "} So while you should definitely catch several of these great HCI sessions, take some time to chill out a bit, too. Feel free to catch me running around and say hi; I'd love to talk with you!
{% endpullquote %}
---

{% figure class:"figure-aside" %}
{% img '{{image_path}}vmworld2017-gamechanger.jpg' alt:'A thematic image for VMworld 2017' %}
{% endfigure %}
This year at VMworld there's a lot of focus on Digital Transformation, which is great news for HCI enthusiasts, because hyper-converged has been leading the charge towards effectively transforming datacenters for some time. For sure, the transformation fire is spreading wildly: [VMware on AWS](https://www.vmware.com/cloud-services/vmware-cloud-aws.html), [VMware Cloud Foundation](https://www.vmware.com/products/cloud-foundation.html), and so many other great initiatives by VMware and technology partners are really changing how datacenters are fundamentally designed and defined. In light of this, the Digital Transformation focus is especially strong in the HCI sessions this year. I'll start with three I'm presenting...

## My VMworld 2017 Sessions[^clickfordetails]
I'm presenting a breakout session, a panel session, and a meet the experts roundtable. I hope to see you there!

<details>
  <summary>
    <h3>Best Practices for vSAN Design: A Real-World Perspective [STO1372BUR]</h3>
    <em>Monday the 28th at 4 and Tuesday the 29th at 3:30</em>
  </summary>
  <p>My esteemed colleague, Josh Fidel, and I will take you on a whirlwind tour through vSAN architecture, starting from use
    cases, assessments, and sizing all the way through go-live. We've only an hour, so there will be plenty of references
    to other fantastic vSAN sessions as well that go deeper on many of the design aspects we'll be covering.</p>
</details>

<details>
  <summary>
    <h3>vSAN Hardware Deep Dive Panel [STO1540PU]</h3>
    <em>Wednesday the 30th at 1</em>
  </summary>
  <p>Join me, VMware Support Technical Director Jeff Taylor, vSAN Dev Engineer Ed Goggin, and hardware guru extraordinaire Dave
    Edwards as we discuss vSAN hardware in fantastic depth. This will be a true geek-out session, as we get into the guts
    and brains of how media, controllers, and the vSAN software work together, and what it all means for your vSAN designs.
    Hosted by the inimitable Ken Werneburg, vSAN Technical Marketing Group Manager.</p>
</details>

<details>
  <summary>
    <h3>Meet the Experts Session: vSAN Day 0 to Infinity [MTE4754U]</h3>
    <em>Tuesday the 29th at 11:15</em>
  </summary>
  <p>In this small-group roundtable session, we can talk about everything from building out to year-5 refreshes. I plan to keep
    the conversation open and direct, led by your questions and interests in vSAN design, lifecycle management, operations,
    and maintenance.</p>
</details>

## Other Great VMworld 2017 HCI Sessions[^clickfordetails]
Here's some other fantastic sessions I'd be remiss to not include. They cover the gamut of HCI, from vSAN build-your-own to the fully turnkey datacenter VxRack SDDC.

<details>
  <summary>
    <h3>The Top 10 Things to Know About vSAN [STO1264BU]</h3>
    <address>
      Duncan Epping, Chief Technologist, VMware<br> Cormac Hogan, Director - Chief Technologist, VMware
    </address>
  </summary>
  <p>In this session, Cormac and Duncan will go over the top 10 things to know about VMware vSAN, starting from the design phase
    to benchmarking and even the operational aspect! Ever wondered what happens when you click rebalance? Should you do this
    once a week? Does it make sense to have two or more disk groups versus a single disk group? Stop wondering and sign up!</p>
    <p>Monday, Aug 28, 5:30</p>
</details>

<details>
  <summary>
    <h3>Key vSAN Use Cases with Customer Case Studies [STO1885BU]</h3>
    <address>
      Nick Wilson, Principal Architect, Verizon Enterprise Services<br> Pooja Virkud, Product Line Marketing Manager, VMware<br>      Rakesh Radhakrishnan, Product Management & Strategy Leader, VMware
    </address>
  </summary>
  <p>What do real-world VMware vSAN deployments look like? What efficiencies and savings have vSAN customers been able to achieve?
    We will explore how organizations with different needs are using vSAN across a variety of use cases, such as business-critical
    and tier 1 applications, virtual desktops, remote offices, management clusters, disaster recovery, and more. With the
    help of insights from customer examples, we will explore how vSAN is a great fit for these use cases, why customers picked
    vSAN over other storage solutions they evaluated, and how they actually implemented the solution. We will look also at
    learnings and recommendations for getting the most out of hyper-converged environments powered by vSAN.</p>
    <p>Monday, Aug 28, 5:30</p>
</details>

<details>
  <summary>
    <h3>VMware Validated Design for SDDC Architecture Deep Dive [PBO1721BU]</h3>
    <address>
      Ryan Johnson, Staff Technical Marketing Architect, VMware<br> Michael Brown, Senior SDDC Integration Architect, VMware
    </address>
  </summary>
  <p>VMware Validated Design for Software-Defined Data Center is a comprehensive and extensively tested blueprint to build and
    operate a software-defined data center (SDDC). The design synthesizes all the elements of a complete SDDC into a standardized
    blueprint that delivers a holistic data center–level approach to deploying and configuring the complete SDDC for a wide
    range of use cases along with detailed guidance on how to efficiently operate it. In this session, you will learn about
    the fundamental design decisions that establish the architecture. From the physical to virtual infrastructure layers,
    through cloud management and operations, you'll gain a clear understanding of the design objectives and how they are
    translated into an architecture that rapidly and repeatedly delivers a ready-to-run SDDC for your business.</p>
    <p>Wednesday, Aug 30, 11:30</p>
</details>

<details>
  <summary>
    <h3>Extreme Performance Series: vSAN Performance Troubleshooting [STO1515BU]</h3>
    <address>
      Amitabha Banerjee, Staff Engineer, VMware<br> Suraj Kasi, Staff Performance Engineer, VMware
    </address>
  </summary>
  <p>Troubleshooting performance issues in a distributed storage solution such as VMware vSAN is complicated due to the interconnectedness
    of storage, networking, and compute. vSAN provides two tools to simplify troubleshooting: vSAN Performance Service and
    vSAN Observer. In this presentation, we will go over both the tools in detail and teach you how to apply a common troubleshooting
    methodology to ensure performance is not a barrier. We'll include details such as tools activation and the types of data
    they collect and, using real-life examples, show how these tools can be used to diagnose performance issues. We will
    also discuss how to mitigate the most common performance issues vSAN customers experience. Walk away from this session
    with tools, knowledge, and confidence that vSAN is easy to support and delivers powerful performance.</p>
    <p>Wednesday, Aug 30, 4:00</p>
</details>

<details>
  <summary>
    <h3>VMware Cloud Foundation Solution Overview [PBO1486BU]</h3>
    <address>
      Alan Hsia, Product Line Marketing Manager, VMware
    </address>
  </summary>
  <p>To respond to the ever-increasing demand for faster innovation, organizations are looking to shift to a more agile, service-oriented
    IT model that leverages the hybrid cloud. Customers are looking for a modern infrastructure that is based on a software-defined
    platform, delivers enterprise capabilities, and establishes a common operational model across public and private. Attend
    this session to learn how VMware Cloud Foundation provides a unified software-defined data center platform for the hybrid
    cloud. As an integrated stack that can be deployed on-premises or run as a service from the public cloud, VMware Cloud
    Foundation establishes a consistent operational model across clouds. We will provide an overview of VMware Cloud Foundation
    and demo key features to showcase how customers can leverage it both on- and off-premises.</p>
    <p>Monday, Aug 28, 1:00</p>
</details>

<details>
  <summary>
    <h3>VxRack SDDC Deep Dive: Inside VxRack SDDC Powered by VMware Cloud Foundation [PBO1064BU]</h3>
    <address>
      Georg Edelmann, Senior Business Development Manager, VMware<br> Jason Marques, Sr. Consultant Engineer, VxRail and
      VxRack SDDC Technical Marketing, Dell EMC
    </address>
  </summary>
  <p>Take a look behind the curtain as experts from Dell EMC and VMware dive deep into the latest turnkey solution for a complete
    software-defined data center (SDDC). See details on how integrated networking, both physical and virtual, along with
    full lifecycle management of hardware and software are the foundation for a complete SDDC infrastructure. Now, VMware
    vSphere, VMware vSAN, and VMware NSX have been combined with the best from Dell EMC into a single agile system.</p>
    <p>Tuesday, Aug 29, 4:00</p>
</details>

<details>
  <summary>
    <h3>Modernizing IT with Server-Centric Architecture Powered by VMware vSAN and VxRail Hyper-Converged Infrastructure Appliance
      [STO1742BU]</h3>
    <address>
      Hanoch Eiron, Product Line Marketing Manager, VMware<br> William Leslie, Technical Product Marketing Manager, Dell
      EMC
    </address>
  </summary>
  <p>With server-centric IT, data centers can offer consumable services that match the public cloud yet offer better security,
    cost efficiency, and performance. This session will discuss the benefits of x86-based server-centric IT and the convergence
    of technologies and industry trends that now enable it. The session will also explain how customers can realize server-centric
    IT with VMware vSAN and the Dell EMC VxRail appliance family, along with detailed analysis demonstrating these claims.</p>
    <p>Thursday, Aug 31, 12:00</p>
</details>

<details>
  <summary>
    <h3>vSAN ReadyNode and Build Your Own Hardware Guidance [STO2095BU]</h3>
    <address>
      John Nicholson, Senior Technical Marketing Manager, VMware<br> Biswapati Bhattacharjee, Senior Product Manager, VMware
    </address>
  </summary>
  <p>VMware vSAN is a core building block of the software-defined data center and the leader in hyper-converged infrastructure.
    Over 200 vSAN ReadyNode options and numerous choices for building your own are available. In this session, we will provide
    insights into how vSAN is designed to handle different types of hardware platforms and the design principles used to
    qualify combinations of I/O controller, boot devices, cache, capacity tier devices (hard disk drives and solid state
    drives), and backplanes in the form of ReadyNode models. We will discuss in detail what you can (and cannot) change in
    a ReadyNode. We will talk about what new generation hardware looks like and its certification on vSAN. We will conclude
    by providing our partners and customers with a "cookbook" that simplifies selection of hardware for vSAN deployment.</p>
    <p>Monday, Aug 28, 4:00</p>
</details>

<details>
  <summary>
    <h3>vSAN Setup & Management the Easy Way! [STO1968BU]</h3>
    <address>
      Jase McCarty, Staff Technical Marketing Architect, VMware
    </address>
  </summary>
  <p>Do you have some vSphere hosts you'd like to run vSAN on? Not sure how to get started? Come to this session to learn about
    how to use a few of the newest features of vSAN 6.6. We'll cover Easy Install, Configuration Assist, Updates and more
    to learn how to get started quickly and easily maintain your vSAN cluster! This session will benefit the newest and oldest
    admins and show them how to get started quickly and easily operate vSAN cluster.</p>
    <p>Tuesday, Aug 29, 1:00</p>
</details>

<details>
  <summary>
    <h3>vSAN 6.6: A Day in the Life of an I/O [STO1926BU]</h3>
    <address>
      John Nicholson, Senior Technical Marketing Manager, VMware<br> Pete Koehler, Sr. Technical Marketing Manager, VMware
    </address>
  </summary>
  <p>This session will discuss what a typical day looks like in the life of an I/O on a solution based on VMware vSAN. How does
    network-based RAID-1 or RAID-5 work? How have checksumming and I/O placement changed since vSAN 6.2? How are "failing"
    devices detected and handled? What improvements have been made to synchronization of data? What about caching, are there
    different layers? Can I control where blocks are stored? And how does all of this influence availability and performance
    of my I/O? The session will cover significant under-the-hood changes that have improved vSAN.</p>
    <p>Thursday, Aug 31, 1:30</p>
</details>

<details>
  <summary>
    <h3>vSAN Beyond the Basics [STO1479BU]</h3>
    <address>
      Eric Knauft, Staff Engineer, VMware<br> Sumit Lahiri, Sr. Product Manager, VMware
    </address>
  </summary>
  <p>vSAN is now ubiquitously deployed for many tier-1 workloads. We will cover a broad spectrum of areas that will help how
    you plan, manage and scale your vSAN deployment. We will cover the design principles covering distributed quorum, placement,
    deduplication, rebuilds/resyncs, congestion, throttling, healing, device decommissioning and others. This session is
    aimed to provide comprehensive overview on vSAN.</p>
    <p>Tuesday, Aug 29, 4:00</p>
</details>

<details>
  <summary>
    <h3>A Real-World Demonstration of Assessing and Sizing a Customer Environment for vSAN [STO1500BU]</h3>
    <address>
      Ankur Huralikoppi, Sr. MTS, VMware<br> Aparna Somaiah, Product Manager, VMware
    </address>
  </summary>
  <p>vSAN as we know it today is being adopted by customers of all Sizes. So how do you, a customer, make an intelligent decision
    about what hardware configuration and platform is suitable for your environment? In this session, we will walk you through
    a live demo of how to assess and size your environment for vSAN and then select a ReadyNode from your preferred vendor.
    The session will cover an overview of all the vSAN Enablement tools and will then have a 20-30 minute live demo where
    we will walk the audience through the process of starting an assessment, then sizing, calculating TCO and finally selecting
    a vSAN ReadyNode.</p>
    <p>Wednesday, Aug 30, 1:00</p>
</details>

<details>
  <summary>
    <h3>A Closer Look at vSAN Networking Design and Configuration Considerations [STO1193BU]</h3>
    <address>
      Cormac Hogan, Director - Chief Technologist, VMware<br> Andreas Scherr, Senior Solution Architect, VMware
    </address>
  </summary>
  <p>In this session, speakers will guide you through some of the most common VMware vSAN network configurations. We will cover
    topics such as the pros and cons of certain NIC teaming options, supported network topologies, L2 versus L3, witness
    traffic separation, two-node ROBO deployments, stretched cluster deployments, and, most importantly, "gotchas" related
    to networking in vSAN deployments.</p>
    <p>Wednesday, Aug 30, 4:00</p>
</details>

<details>
  <summary>
    <h3>Understanding the Availability Features of vSAN [STO1179BU]</h3>
    <address>
      Jeff Hunter, Staff Technical Marketing Architect - Storage and Availability, VMware<br> GS Khalsa, Sr. Technical Marketing
      Manager, VMware
    </address>
  </summary>
  <p>Application uptime and resiliency to failure are key factors with any hyper-converged infrastructure storage solution.
    Before choosing a platform, it is important to understand its ability to recover from issues such as disk, host, and
    even entire site failures. This session will focus specifically on the availability features of VMware vSAN. It is a
    technical discussion that begins with a quick explanation of how vSAN stores virtual machine objects. In addition, we
    will look at storage policy-based management, as this is the primary method for managing virtual machine resiliency,
    as well as RAID 1 mirroring and RAID 5/6 erasure coding and how these failure protection methods affect placement of
    vSAN objects. We will also provide details on vSAN fault domains and stretched clusters and finish with a discussion
    about data protection.</p>
    <p>Monday, Aug 28, 5:30</p>
</details>

<details>
  <summary>
    <h3>vSphere Encryption for Virtual Machines and vSAN Encryption Deep Dive [STO1960BU]</h3>
    <address>
      Mike Foley, Sr. Technical Marketing Architect, VMware<br> Jase McCarty, Staff Technical Marketing Architect, VMware
    </address>
  </summary>
  <p>Virtual machine encryption? VMware vSAN encryption? Which one do I use? When should I use one versus the other? Can I use
    both? Should I? VMware vSphere 6.5 and vSAN 6.6 introduced these cool features, but as with any new feature, how you
    use them is the most important thing. Security shouldn’t be hard. Let us show you how easy incorporating virtual machine
    encryption and/or vSAN encryption into your daily operations really is.</p>
    <p>Tuesday, Aug 29, 4:00</p>
</details>

<details>
  <summary>
    <h3>Successful vSAN Stretched Clusters [STO1118BU]</h3>
    <address>
      GS Khalsa, Sr. Technical Marketing Manager, VMware<br> Jase McCarty, Staff Technical Marketing Architect, VMware
    </address>
  </summary>
  <p>This session will provide a deep dive into using stretched clusters with VMware vSAN. Topics include the foundation of
    vSAN stretched clusters, the latest updates, recommendations, and failure scenarios. For those interested in implementing
    stretched clusters with vSAN, you won't want to miss this session.</p>
    <p>Wednesday, Aug 30, 2:30</p>
</details>

<details>
  <summary>
    <h3>Architecting Site Recovery Manager to Meet Your Recovery Goals [STO2063BU]</h3>
    <address>
      GS Khalsa, Sr. Technical Marketing Manager, VMware
    </address>
  </summary>
  <p>VMware customers have a variety of options available when deciding how to architect their VMware Site Recovery Manager
    deployment. Optimal configuration of Site Recovery Manager depends on whether customers are prioritizing speed of recovery
    or want fine-grained control of their disaster recovery plans. This session, which will include demos, covers organization
    of protection groups and recovery plans, supported Site Recovery Manager topologies, and specific suggestions for improving
    virtual machine recovery time performance. If you are interested in making the most of your Site Recovery Manager deployment
    to meet your business's disaster recovery needs, and you want to know how to get the most from the latest Site Recovery
    Manager capabilities, this is the session for you.</p>
    <p>Thursday, Aug 31, 1:30</p>
</details>

<details>
  <summary>
    <h3>Successful vSAN for Remote Offices and Branch Offices [STO1994BU]</h3>
    <address>
      Matthew Douglas, Director of Cloud & Solutions Architecture, Smithfield Foods<br> Kristopher Groh, Senior Product Manager,
      VMware<br> Jase McCarty, Staff Technical Marketing Architect, VMware
    </address>
  </summary>
  <p>Are you looking to deploy VMware vSAN to one or more remote or branch offices? Come to this session to see how easy and
    cost-effective VMware vSphere and vSAN can be for your remote office branch office (ROBO) requirements. Have a few locations?
    A few hundred? More? See how vSphere and vSAN can be the most cost-effective ROBO combination.</p>
    <p>Monday, Aug 28, 5:30</p>
</details>

<details>
  <summary>
    <h3>Technical Preview of Integrated Data Protection for vSAN: A Native, Scalable Solution to Safeguard Data on vSAN [STO1770BU]</h3>
    <address>
      Shobhan Lakkapragada, Director of Product Management, VMware<br> Michael Ng, Senior Product Manager, VMware
    </address>
  </summary>
  <p>VMware vSAN is widely used for business-critical applications that store valuable production data for businesses. It is
    imperative that this data be protected to ensure quick recovery in the event of data loss or an infrastructure failure.
    Join us for a preview of a planned vSAN solution that integrates data protection seamlessly into vSAN. We will discuss
    various aspects of this solution, including scalable snapshots, archiving to secondary storage, policy-based data protection,
    recovery workflows, and much, much more.</p>
    <p>Wednesday, Aug 30, 11:30</p>
</details>

<details>
  <summary>
    <h3>Conducting a Successful vSAN 6.x Proof of Concept [STO2652BU]</h3>
    <address>
      David Boone, Staff Solutions Architect, VMware<br> Dave Morera, Senior Solutions Architect, VMware
    </address>
  </summary>
  <p>Proper Planning Prevents Poor Performance… Proper design, sizing and planning among other aspects are key to success in
    many areas, and VMware vSAN is no exception to the rule. The importance and process of a vSAN Proof of Concept based
    on the vSAN POC Guide is a simple way to achieve success on implementations. The POC process can also be used as a guideline
    for vSAN environments other than POCs; hence, it is important to make the audience aware of the processes and procedures
    that will be of highly importance during vSAN deployments. Using hardware as the foundation, its criticality and effects
    on the product will be explored, as well as aspects to be aware of during the planning and procurement phase. Other tasks
    such as architecture, design recommendations, failure testing as well as performance testing will be covered.</p>
    <p>Thursday, Aug 31, 1:30</p>
</details>

<details>
  <summary>
    <h3>vSAN Technical Deep Dive [STO2986BU]</h3>
    <address>
      Eric Knauft, Staff Engineer, VMware<br> Swaroop Dutta, Director Product Management, VMware
    </address>
  </summary>
  <p>In this session, we will provide a technical deep dive into the core vSAN architecture. VMware vSAN is hypervisor-converged
    infrastructure designed and optimized for vSphere virtual infrastructure. We will deep dive into installing, managing,
    and monitoring vSAN and also cover in detail its technical aspects and core architecture.</p>
    <p>Wednesday, Aug 30, 1:00</p>
</details>

<details>
  <summary>
    <h3>vSAN Networking and Design Best Practices [STO3276GU]</h3>
    <address>
      John Nicholson, Senior Technical Marketing Manager, VMware
    </address>
  </summary>
  <p>Join this session to discuss top tricks to a successful outcome with vSAN. Topics will include: Throughput requirements,
    removal of Multicast, network security, performance, and layer 2 vs. layer 3. In addition any questions you have on vSAN.</p>
    <p>Wednesday, Aug 30, 11:30</p>
</details>

<details>
  <summary>
    <h3>vSAN Troubleshooting Deep Dive [STO1315BU]</h3>
    <address>
      Javier Menendez, Technical Instructor, VMware<br> Francis Daly, Staff Technical Training Specialist, VMware
    </address>
  </summary>
  <p>Want to know how VMware Technical Support troubleshoots issues? Join us for this session. We'll take a look at broken VMware
    vSAN clusters using actual cases from our customers. Speakers will cover which tools VMware uses along with VMware's
    troubleshooting approach to data unavailability situations in vSAN clusters. We will also discuss common mistakes made
    when setting up vSAN clusters along with architectural best practices and how to resolve issues with confidence.</p>
    <p>Thursday, Aug 31, 1:30</p>
</details>

<details>
  <summary>
    <h3>Deep Dive on Encrypting your Data-at-Rest with vSAN [STO1451BU]</h3>
    <address>
      Wenguang Wang, Sr. Staff Engineer, VMware<br> Sumit Lahiri, Sr. Product Manager, VMware
    </address>
  </summary>
  <p>With vSAN 6.6, we introduced software encryption for data-at-rest. This is the industry’s first native HCI security solution
    and fundamentally changes the game in the way encrypted clusters are deployed and managed. In this session will provide
    you a detailed overview of how vSAN is designed grounds up to safeguard data-at-rest. We will begin with the cryptographic
    principles applied in encryption and delve into the design principles and key use cases of deploying and managing your
    encrypted vSAN cluster.</p>
    <p>Tuesday, Aug 29, 11:30</p>
</details>

<details>
  <summary>
    <h3>vSAN Performance Tips [STO3189GU]</h3>
    <address>
      Amitabha Banerjee, Staff Engineer, VMware<br> Suraj Kasi, Staff Performance Engineer, VMware
    </address>
  </summary>
  <p>Are you really getting the most out of your VMware vSAN? Troubleshooting performance issues in a distributed storage solution
    such as VMware vSAN is complicated due to the interconnectedness of storage, networking, and computing. Join us to discuss
    testing and tuning insights and lessons learned so you can walk away with knowledge and confidence that vSAN is easy
    to support and delivers powerful performance.</p>
    <p>Tuesday, Aug 29, 4:00</p>
</details>

<details>
  <summary>
    <h3>vSAN Technical Customer Panel [STO2615PU]</h3>
    <address>
      Peter Keilty, Office of the CTO, Global Field | SE Manager SDS East, VMware Joachim Heppner, Director, Virtualization Engineering
      Services, Sanofi Michael DiBenedetto, Director, IT, Sekisui Diagnostics Alexander Szwez, Sr Sys Eng, Travelers
    </address>
  </summary>
  <p>Join us for a technical customer panel where four vSAN customers will take the stage for a panel discussion about their
    unique vSAN use cases each meeting common outcomes including: Cost-efficient performance, storage convergence, reduced
    complexity, and increased scalability. Each customer has a unique perspective on how vSAN has helped modernize their
    Data Centers. Peter Keilty from VMware’s Office of the CTO, America’s Field will moderate the session, keep the discussion
    interesting, and solicit audience participation to make this a truly interactive session.</p>
    <p>Wednesday, Aug 30, 8:30</p>
</details>

## A Really Cool Analysis

Lastly, [Anthony Spiteri from Veeam](https://twitter.com/anthonyspiteri) provided a really cool [breakdown of VMworld 2017 session submissions and acceptance rates](https://anthonyspiteri.net/vmworld-2017-session-breakdown-and-analysis/). I thought it was pretty interesting.

[^clickfordetails]: Click on any of the session items below for a brief description. (Or if you're using a Microsoft Browser, just, uh, look down there.)
