# Base Settings
title: Tohuw.Net
email: ron@tohuw.net
description: >
  Ron Scott-Adams is an HCI Systems Engineer with a passion for
  creating and explaining well-designed data centers
  and next-generation application platforms.
baseurl: ''
url: "https://tohuw.net"
timezone: America/Chicago

# Metadata
language: en
author: Ron Scott-Adams
subtitle: Clouds, Chaos, & Yak Shaving

# Navigation
menuicons: true
menuitem_articles: true
menuitem_tags: true
menuitem_linkpage: true
menuitem_social: true
subtitle_link: "/about"
topic_link: true

# Search
search_enabled: true
search_placeholder: "&#xf002;"

# Tags
tag_page_layout: tag_page
tag_page_dir: tag
tag_permalink_style: pretty
ignored_tags:
topics_are_tags: true

# Social
social_title:
rss_title: icon
twitter_title: icon
twitter_username: tohuw
twitter_hashtag: :slug
linkedin_title: icon
linkedin_username: tohuw
# github_title: icon
# github_username:  tohuw
gitlab_title: icon
gitlab_username: tohuw
facebook_title: icon
facebook_username: tohuw
disqus_shortname: tohuw

# Recent Posts List
recentposts_display: true
recentposts_title: More, Here
recentposts_longcount: 25
recentposts_shortcount: 3
recentposts_meta: true
recentposts_moremeta: false

# Blogroll
blogroll_display: true
blogroll_title: More, Elsewhere
blogroll_footer_count: 4
blogroll:
    - name: Yellow Bricks
      subtitle: Virtualization Insights from a Chief Technologist at VMware
      url: https://yellow-bricks.com
    - name: Cormac Hogan
      subtitle: Amazing In-Depth vSAN, Virtualization, & CNA Coverage
      url: https://cormachogan.com
    - name: Virtually Ghetto
      subtitle: A Virtual Automation & Lab Genius
      url: https://virtuallyghetto.com
    - name: Dadhacker
      subtitle: Atari, Apple, Microsoft... He's Seen Things
      url: https://dadhacker.com
    - name: TinkerTry
      subtitle: Brilliant Datacenter Labbing & Theorycrafting
      url: https://tinkertry.com
    - name: Virtual Ramblings
      subtitle: Really Smart & Clear Thoughts on Storage Virtualization
      url: https://thenicholson.com
    - name: Live Virtually
      subtitle: Immediately Applicable Virtualization Notes
      url: https://livevirtually.net
    - name: vSphere Land
      subtitle: A Premier Virtualization Resource
      url: https://vsphere-land.com
    - name: Virtually Speaking Podcast
      subtitle: Awesome Storage & Availability Discussions
      url: https://vspeakingpodcast.com
    - name: VMware Communities Roundtable
      subtitle: All Things Abuzz in the VMware Community
      url: http://www.talkshoe.com/talkshoe/web/talkCast.jsp?masterId=19367
    - name: Virtualization is Life!
      subtitle: A savvy take on VMware vSphere and the SDDC
      url: https://anthonyspiteri.net

# Front Matter Defaults
defaults:
  -
    scope:
      path: ""
      type: "posts"
    values:
      layout: "post"
  -
    scope:
      path: ""
      type: "pages"
    values:
      layout: "page"

# Layout: readtime
readtime_display: true
readtime_title: icon

# Layout: nowlistening
nowlistening_display: true
nowlistening_title: icon

# License
license_display: true
license_title: "CC BY-NC-SA 4.0"
license_url: /rights/

# Analytics
piwik_url: stats.tohuw.net
piwik_siteid: 1

#Related Posts
relatedposts_display: false

# Build settings
include:
  - /_pages
exclude:
  - README.md
  - LICENSE.md
  - Gemfile
  - Gemfile.lock
permalink: /articles/:title

compress_html:
  clippings: all
  comments: ["<!-- ", " -->"]
  endings: all
  ignore:
    envs: [development]
  blanklines: false
  profile: false

# Plugin: jekyll-assets
assets:
  assets:
    - "*.woff"
    - "*.woff2"
    - "theme/*"
  digest: false
  autoprefixer:
    browsers: ["last 2 versions","> 5%","IE 9"]
